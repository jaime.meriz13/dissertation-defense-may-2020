\beamer@sectionintoc {1}{Dissertation Defense Overview}{2}{0}{1}
\beamer@sectionintoc {2}{Rock Fractures as Geophysical Phenomenon}{3}{0}{2}
\beamer@sectionintoc {3}{Discrete Fracture Network Modeling}{5}{0}{3}
\beamer@sectionintoc {4}{Objectives Of Optimizing Graphs}{10}{0}{4}
\beamer@sectionintoc {5}{Discrete Fracture Networks as Graphs}{11}{0}{5}
\beamer@sectionintoc {6}{Fordward and Inverse Models on the Graph}{14}{0}{6}
\beamer@sectionintoc {7}{Results of Calibrating Pressures}{23}{0}{7}
\beamer@sectionintoc {8}{Results of Calibrating Weights}{29}{0}{8}
\beamer@sectionintoc {9}{Breakthrough Curves}{36}{0}{9}
\beamer@sectionintoc {10}{First Passage Times}{42}{0}{10}
\beamer@sectionintoc {11}{Summary of Advancements and Conclusion}{44}{0}{11}
